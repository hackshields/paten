## Install and start Ethereum nodes

### Download and install geth
Download and install on the official website.

### Configure and start geth
Copy the contents of the project geth folder to the installation directory, and then execute node1.bat and node2.bat to start two pre-configured Ethereum nodes.
If you want to generate a new blockchain, you can customize the CustomGenesis.json file to modify the initial block information, delete the blockchaindata and blockchaindata1 directories, and then execute myinit1.bat and myinit2.bat to initialize the nodes.

## Resolving Python project dependencies
Since the project uses Python 3.5, Flask, and Ethereum RPC, the dependency package needs to be installed, and the dependency information is written in requirements.txt.
There are two ways:
1. Use the virtual environment (windows x64, python3.5) under the configured venv directly, no need to download separately. Just execute `venv \ Scripts \ active.bat`.
2. Use pip to resolve dependencies: Run `pip install -r requirements.txt` to automatically download and install dependencies.

## Database configuration
You can choose to use a database such as sqlite or mysql in config.py.
If you use mysql, you need to replace the original database connection string with the connection string including the account password, and create a corresponding database in the data control. The table will be automatically created by the system.
If you use sqlite, the system will automatically create files and tables.

## Compile and deploy Ethereum contract
The project has compiled the smart contract and stored the contract information: ABI, bytecode and contract address in contract.json. But you can also execute `python3 manage.py deploy_contract` to redeploy and publish a new contract, and the system will automatically save the new contract address back to the file.
During the secondary development, the compiled contract binary code and ABI can be replaced and re-deployed or the ABI and address can be directly used.

## Startup project
Execute `python3 manage.py runserver` to start the project and listen on port 5000 by default.

